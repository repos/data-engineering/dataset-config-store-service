import { execSync } from "child_process";
import ServiceUtils from "@tchin/service-utils";

export default defineNitroPlugin(async (nitroApp) => {
  // Plugins in Nitro (currently) load async, so we initialize here instead of serviceUtils so we can log
  await ServiceUtils.initialize({envName: process.env.NODE_ENV});
  const logger = ServiceUtils.getLogger();
  logger.log(
    "info",
    `Using Config Store Backend: ${useRuntimeConfig().configStore?.backend}`
  );
  if (
    useRuntimeConfig().configStore?.backend !== "repo" ||
    !useRuntimeConfig().configStore?.pullRepo
  ) {
    return;
  }
  const configRepoUrl = useRuntimeConfig().configStore?.configRepoProjectPath;
  if (!configRepoUrl) {
    logger.error("A git repo must be specified under configStore.configRepoProjectPath in nitro.config.ts");
    return;
  }
  const gitUrl = `https://gitlab.wikimedia.org/${configRepoUrl}.git`;
  const pullDirectory = useRuntimeConfig().configStore?.gitParentDirectory
    ? useRuntimeConfig().configStore?.gitParentDirectory
    : process.env.PWD;
  execSync(`rm -rf config && git clone ${gitUrl} config`, {
    stdio: [0, 1, 2],
    cwd: pullDirectory,
  });
  logger.log("info", "Finished cloning config store");
});
