//https://nitro.unjs.io/config
export default defineNitroConfig({
  runtimeConfig: {
    cacheMaxAge: 60, // Seconds
    configStore: {
      /**
       * 'repo' or 'api'
       * 'repo' clones the repo locally and caches it.
       * 'api' calls out to the GitLab api and caches the responses.
       */
      backend: "repo",
      /**
       * The GitLab config repo
       */
      configRepoProjectPath: "tchin/config-store-poc",
      /**
       * If the backend is set to `"repo"`, this defines the writeable directory that the config values repo is cloned into.
       * Defaults to process.env.PWD.
       */
      gitParentDirectory: undefined,
      /**
       * If the backend is set to 'repo', this toggles whether to pull
       * the repo at server startup.
       *
       * Set this to false if the repo already exists locally.
       */
      pullRepo: true,
      /**
       * If backend is set to repo, this stops all caching and timetravelling
       * so that you can develop config values locally.
       * Setting this to true implicitly makes pullRepo false.
       */
      configDevMode: false,
    },
  },
  // Production
  storage: {
    cache: {
      driver: "lruCache",
    },
    configStore: {
      driver: "lruCache",
    },
  },
  // Development
  devStorage: {
    configStore: {
      driver: "fs",
      base: "./data/configStore",
    },
    // Cache uses filesystem
  },
  devServer: {
    watch: ["service-utils.config.yaml"],
  },
});
