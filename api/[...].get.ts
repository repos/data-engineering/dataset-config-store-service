import { getConfigStore } from "../utils/config";
import ServiceUtils, { MetricType } from "@tchin/service-utils";

// Metrics are only made once and then cached via the name
const getRouterMetrics = () => ServiceUtils.getMetrics().makeMetric({
  type: MetricType.HISTOGRAM,
  name: "router",
  prometheus: {
    name: "nitro_config_request_duration_seconds",
    help: "config request duration handled in seconds",
    staticLabels: ServiceUtils.getMetrics().getServiceLabel(),
    buckets: [0.01, 0.05, 0.1, 0.3, 1],
  },
  labels: {
    names: ["path", "method", "status"],
    omitLabelNames: true,
  },
});

export default defineEventHandler(async (event) => {
  const metric = getRouterMetrics();
  const startTime = Date.now();

  const configPath = getRequestURL(event).pathname.replace("/api/", "");

  const logger = ServiceUtils.getLogger();
  logger.log("info", `Config requested: ${configPath}`);

  const { git_sha } = getQuery<{ git_sha: string }>(event);

  const configStore = getConfigStore(logger);

  try {
    const content = await configStore.getNormalizedConfig(configPath, {
      gitSHA: git_sha,
    });
    metric.endTiming(startTime, [event.path || "root", "GET", "200"]);
    return content;
  } catch (e) {
    logger.log("error", e);
    if (e.code === "ENOENT") {
      sendNoContent(event);
      metric.endTiming(startTime, [event.path || "root", "GET", "204"]);
    }
    sendError(event, e);
    metric.endTiming(startTime, [event.path || "root", "GET", "500"]);
  }
});
