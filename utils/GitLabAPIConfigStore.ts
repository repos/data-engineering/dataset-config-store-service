import { ConfigStore, ConfigOptions } from "./ConfigStore";
import type { Storage, StorageValue } from "unstorage";
import { parse } from "yaml";
import winston from "winston";

const fetchConfigValues = async (path: string, gitSHA?: string) => {
  return await fetchRawFileFromGitLab(`values/${path}/values.yaml`, gitSHA);
};

/**
 *
 * @param path $schema path (with preceding `/`) i.e. `/test/1.0.0`
 * @param gitSHA
 * @returns
 */
const fetchConfigSchema = async (path: string, gitSHA?: string) => {
  return await fetchRawFileFromGitLab(`jsonschema${path}.yaml`, gitSHA);
};

const fetchRawFileFromGitLab = defineCachedFunction(
  async (filepath: string, gitSHA?: string) => {
    const template = `https://gitlab.wikimedia.org/api/v4/projects/${encodeURIComponent(
      useRuntimeConfig().configStore.configRepoProjectPath
    )}/repository/files/${encodeURIComponent(filepath)}/raw?ref=${
      gitSHA ? gitSHA : "HEAD"
    }`;
    return await $fetch<string>(template);
  },
  {
    maxAge: useRuntimeConfig().cacheMaxAge,
    name: "rawFiles",
    getKey: (filepath, gitSHA) => `${filepath}/${gitSHA ? gitSHA : "HEAD"}`,
    swr: false,
  }
);

export class GitLabAPIConfigStore extends ConfigStore {
  public gitLabProjectPath: string;
  constructor(
    storage?: Storage<StorageValue>,
    logger?: winston.Logger,
    gitLabProjectPath?: string
  ) {
    super(storage);
    this.gitLabProjectPath =
      gitLabProjectPath ?? useRuntimeConfig().configStore.configRepoProjectPath;
    if (!this.gitLabProjectPath) {
      logger.error(
        "A git repo must be specified under configStore.configRepoProjectPath in nitro.config.ts"
      );
      throw new Error("Config store repo not set.");
    }
    logger.log(
      "info",
      `GitLabAPIConfigStore instantiated with repo from ${this.gitLabProjectPath}`
    );
  }

  async getNormalizedConfig(
    path: string,
    options?: ConfigOptions
  ): Promise<object> {
    const configYaml = await fetchConfigValues(path, options.gitSHA);
    const config = parse(configYaml);
    const schemaPath = config.$schema;
    if (!schemaPath) {
      throw new Error("Schema not found");
    }
    const configSchemaYaml = await fetchConfigSchema(
      schemaPath,
      options.gitSHA
    );
    const configSchema = parse(configSchemaYaml);

    const normalizaConfig = ConfigStore.normalizeConfig(configSchema, config);
    return normalizaConfig;
  }
}
