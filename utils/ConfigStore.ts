import type { Storage, StorageValue } from "unstorage";
import winston from "winston";

export type ConfigStoreBackend = "api" | "repo";

export interface ConfigOptions {
  gitSHA?: string;
}

type ConfigSchemaProperties = {
  properties: {
    [key: string]: {
      type?: string;
      const?: any;
      default?: any;
      properties?: { [key: string]: { type: string } };
      items?: { type: string };
    };
  };
};

export abstract class ConfigStore {
  public storage: Storage<StorageValue>;
  public logger: winston.Logger;
  constructor(storage?: Storage<StorageValue>, logger?: winston.Logger) {
    this.storage = storage ?? useStorage("configStore");
    this.logger = logger;
  }

  abstract getNormalizedConfig(
    path: string,
    options?: ConfigOptions
  ): Promise<object>;

  /**
   * Sets defaults and consts specified in the configSchema to the config.
   *
   * @param configSchema
   * @param config
   * @returns
   */
  static normalizeConfig(
    configSchema: ConfigSchemaProperties,
    config: object
  ): object {
    let normalizedConfig: { [key: string]: any } = {};

    for (const key in configSchema.properties) {
      const property = configSchema.properties[key];
      if (property.const) {
        normalizedConfig[key] = property.const;
        continue;
      }

      switch (property.type) {
        case "object":
          if (property.default && config.hasOwnProperty(key)) {
            normalizedConfig[key] = { ...property.default, ...config[key] };
          } else if (property.default) {
            normalizedConfig[key] = property.default;
          } else {
            normalizedConfig[key] = config.hasOwnProperty(key)
              ? config[key]
              : property.default;
          }
          break;
        default:
          normalizedConfig[key] = config.hasOwnProperty(key)
            ? config[key]
            : property.default;
      }
    }

    return normalizedConfig;
  }
}
