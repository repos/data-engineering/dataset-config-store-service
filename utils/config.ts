import type { Storage, StorageValue } from "unstorage";
import { GitLabAPIConfigStore } from "./GitLabAPIConfigStore";
import { GitRepoConfigStore } from "./GitRepoConfigStore";
import { ConfigStore, ConfigStoreBackend } from "./ConfigStore";
import winston from "winston";

// TODO
export const getCachedApiSpec = defineCachedFunction(async () => {}, {
  maxAge: useRuntimeConfig().cachedMaxAge, // Seconds
  name: "apiSpec",
  getKey: () => "apiSpec",
});

let configStore: ConfigStore;

export const instantiateConfigStore = (
  backend: ConfigStoreBackend,
  store: Storage<StorageValue>,
  logger?: any
) => {
  switch (backend) {
    case "api":
      return new GitLabAPIConfigStore(
        store,
        logger,
        useRuntimeConfig().configStore?.configRepoProjectPath
      );
    case "repo":
      return new GitRepoConfigStore(
        store,
        logger,
        useRuntimeConfig().configStore?.gitParentDirectory,
        useRuntimeConfig().configStore?.configDevMode
      );
    default:
      throw new Error("Config Store undefined");
  }
};

/**
 * Returns a ConfigStore configured using nitro.config.ts
 * and a logger from service-runner or service-utils.
 * It caches and returns the same ConfigStore instance,
 * even if the parameter changes.
 * @param logger 
 * @returns 
 */
export const getConfigStore = (logger?: winston.Logger) => {
  if (configStore) return configStore;
  configStore = instantiateConfigStore(
    useRuntimeConfig().configStore?.backend,
    useStorage("configStore"),
    logger
  );
  return configStore;
}