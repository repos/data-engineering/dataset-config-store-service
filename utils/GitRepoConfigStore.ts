import { ConfigStore, ConfigOptions } from "./ConfigStore";
import type { Storage, StorageValue } from "unstorage";
import fs from "node:fs";
import { parse } from "yaml";
import { execSync } from "child_process";
import path from "node:path";
import winston from "winston";

const execCmd = (cmd: string, dir: string, logger: winston.Logger) => {
  logger.log("verbose", `Executing command: ${cmd} in ${dir}`);
  execSync(cmd, {
    //   stdio: [0, 1, 2],
    stdio: "ignore",
    cwd: dir,
  });
};

export const updateConfigRepo = defineCachedFunction(
  (dir: string, logger: winston.Logger) => {
    logger.log("info", `Repulling at ${dir}`);
    execCmd(`git checkout main && git pull`, dir, logger);
    return dir;
  },
  {
    maxAge: useRuntimeConfig().cacheMaxAge,
    name: "configRepo",
    getKey: (dir: string, logger?: winston.Logger) => dir,
    swr: false,
  }
);

export class GitRepoConfigStore extends ConfigStore {
  public gitDirectory: string;
  public configDevMode: boolean;
  constructor(
    storage?: Storage<StorageValue>,
    logger?: winston.Logger,
    gitParentDirectory?: string,
    configDevMode?: boolean
  ) {
    super(storage);
    this.gitDirectory = path.resolve(
      gitParentDirectory ? gitParentDirectory : process.env.PWD,
      "config"
    );
    this.configDevMode = configDevMode;
    this.logger = logger;
    logger.log(
      "info",
      `GitRepoConfigStore instantiated with repo in ${this.gitDirectory} and configDevMode: ${configDevMode}`
    );
  }

  getNormalizedConfig = async (
    path: string,
    options?: ConfigOptions
  ): Promise<object> => {
    let fileDir = this.gitDirectory;
    if (!this.configDevMode) {
      updateConfigRepo(this.gitDirectory, this.logger);

      if (options.gitSHA) {
        fileDir = `${this.gitDirectory}/${options.gitSHA}`;
        const cacheKey = `${fileDir}/${path}`;
        if (await this.storage.hasItem(cacheKey)) {
          return await this.storage.getItem(cacheKey);
        }
        execCmd(
          `git worktree add ${fileDir} ${options.gitSHA}`,
          this.gitDirectory,
          this.logger
        );
      }
    }

    const configYaml = fs.readFileSync(
      `${fileDir}/values/${path}/values.yaml`,
      "utf8"
    );
    const config = parse(configYaml);

    const schemaPath = config.$schema;
    if (!schemaPath) {
      throw new Error("Schema not found");
    }

    const configSchemaYaml = fs.readFileSync(
      `${fileDir}/jsonschema${schemaPath}.yaml`,
      "utf8"
    );
    const configSchema = parse(configSchemaYaml);

    const normalizaConfig = ConfigStore.normalizeConfig(configSchema, config);

    if (options.gitSHA && !this.configDevMode) {
      await this.storage.setItem(`${fileDir}/${path}`, normalizaConfig);
      execCmd(`rm -rf ${fileDir}`, this.gitDirectory, this.logger);
      execCmd(`git worktree prune`, this.gitDirectory, this.logger);
    }
    return normalizaConfig;
  };
}
