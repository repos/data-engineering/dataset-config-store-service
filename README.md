# Config Store Nitro API POC

This is a POC meant to serve the [Config Store POC](https://gitlab.wikimedia.org/tchin/config-store-poc). This POC uses [Nitro](https://nitro.unjs.io/) for fast development. Nitro may/may not be the correct choice. Expect things to break as things get hammered out.

## Features

- Caching of config store (if backend is set to `"repo"`)
- LRU Caching of config values

## Local Development

1. Download a modern version of NodeJS (I suggest using [nvm](https://github.com/nvm-sh/nvm))

2. `npm install`

3. `npm run dev`

## Config Store API Configuration

Configuration of the store is done in `nitro.config.ts`.

| Variable | Default | Notes |
| - | - | -|
|`runtimeConfig.cacheMaxAge`| `undefined` | Max age that a cache can be |
|`runtimeConfig.configStore.backend`| `undefined` | `"repo"` or `"api"`. Where the API gets the config values and schemas. Defines whether to locally clone the repo or hit the GitLab API. |
|`runtimeConfig.configStore.configRepoProjectPath`| `undefined` | The URL path of the repo that stores config values on GitLab. No trailing space. |
|`runtimeConfig.configStore.gitParentDirectory`| `process.env.PWD` | If the backend is set to `"repo"`, this defines the writeable directory that the config values repo is cloned into. |
|`runtimeConfig.configStore.pullRepo`| `undefined` | If the backend is set to `"repo"`, this toggles whether to pull it at startup. |
|`runtimeConfig.configStore.configDevMode`| `undefined` | If the backend is set to `"repo"`, this prevents caching and timetravelling so you can develop config values locally. Setting this to `true` implies `pullReppo` is `false` |

## Lifecycle

1. Server starts up and runs the code defined in `/plugins` alphabetically.

    - For `"repo"` backend, it initially clones the Config Store POC into the directory specified in `gitParentDirectory` in `nitro.config.ts` in a folder `/config`.
2. On every `/api/` request:

    1. Treat the url path as a path inside `/values` in the Config Store POC and fetches it.

        For `"api"` backend, caching is done on every GitLab endpoint hit.

        For `"repo"` backend, caching is really just checking at request time the lsat time the repo was pulled and then pulling it again.

    2. Treats the `$schema` property in the fetched config values as a path in `/jsonschema` in the Config Store POC and fetches it
    and applies defaults and constants

    3. Returns the config, `204` if config isn't found, or `500` if something else went wrong.

## Deploy to NodeJS

1. `npm run build`

2. `node .output/server/index.mjs`

## Build a Docker image from the Blubber file

`DOCKER_BUILDKIT=1 docker build --target production-run -t datasets-config -f .pipeline/blubber.yaml .`

## Caveats

With the `runtimeConfig.configStore.backend` variable in `nitro.config.ts` set to `repo`, the config repo is by default pulled into `process.env.PWD/config`. It needs to be a writable location.
Be weary of this. Make sure you know where you're running the server.

## Overriding Runtime Variables

Runtime variable defaults are defined in `nitro.config.ts`.
They can be overridden using a `.env` file replacing camelCase with upper case and underscores i.e. `apiToken` to `API_TOKEN`.
> In production, prefix all overrides with `NITRO_`

## TODO
- [ ] Dynamically generate OpenAPI Spec from Config Store POC
- [x] Bypass repo pulling if it already exists (i.e. deployed on k8 with a prepopulated emptyDir)
- [ ] Load repo completely in-memory for complex config querying (?)
- [ ] Fallback mechanism? First try local repo, then hit api?