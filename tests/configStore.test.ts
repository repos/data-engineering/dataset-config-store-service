import { describe, expect, it } from "vitest";
import { ConfigStore } from "../utils/ConfigStore";

describe("ConfigStore", () => {
  describe("normalizaConfig", () => {
    it("sets consts correctly", () => {
      const configSchema = {
        properties: {
          testConst: {
            const: "this is a const",
          },
        },
      };

      expect(ConfigStore.normalizeConfig(configSchema, {})).toEqual({
        testConst: "this is a const",
      });
      expect(
        ConfigStore.normalizeConfig(configSchema, {
          testConst: "override this",
        })
      ).toEqual({
        testConst: "this is a const",
      });
    });

    it("sets defaults correctly", () => {
      const configSchema = {
        properties: {
          testString: {
            type: "string",
            default: "default value",
          },
          testObject: {
            type: "object",
            default: {
              testString: "default value",
              testNumber: 42,
            },
            properties: {
              testString: { type: "string" },
              testNumber: { type: "number" },
            },
          },
          testArray: {
            type: "array",
            default: [1, 2, 3],
            items: { type: "number" },
          },
        },
      };

      expect(ConfigStore.normalizeConfig(configSchema, {})).toEqual({
        testString: "default value",
        testObject: {
          testString: "default value",
          testNumber: 42,
        },
        testArray: [1, 2, 3],
      });

      expect(
        ConfigStore.normalizeConfig(configSchema, {
          testString: "other value",
          testObject: { testNumber: 24 },
          testArray: [4, 2],
        })
      ).toEqual({
        testString: "other value",
        testObject: {
          testString: "default value",
          testNumber: 24,
        },
        testArray: [4, 2],
      });
    });
  });
});
